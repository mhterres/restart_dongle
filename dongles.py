#!/usr/bin/env python
# -*- coding: utf-8 -*-

# dongles.py
# Operações com Dongle
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-10-10
#

import time
import shlex
import subprocess

def run_command(cmd):
	process =subprocess.Popen(
			args=cmd,
			stdout=subprocess.PIPE
	)

	data=[]

	for line in process.stdout.readlines():
		data.append(line.replace("\n",""))

	return data

def verifyDongles(data,cfg,DB,dryrun):

	validStatus=["free","outgoing","gsm not","sms"]
	validStatusStr="free,outgoing,gsm,sms"

	dongles=run_command([cfg.RASTERISK_PATH,"-rx","dongle show devices"])
	calls=run_command([cfg.RASTERISK_PATH,"-rx","core show channels concise"])

	counter=0

	validDongles=[]

	data=time.strftime("%Y-%m-%d %H:%M:%S")
	dongles_status=""

	restarted=False
	rChannels=""
	restartedChannels=[]

	if cfg.debug=="1":

		cfg.logs.writeDebugLog(data)
		cfg.logs.writeDebugLog("Status dos Dongles")
		cfg.logs.writeDebugLog("******************")

	for dongle in dongles:
			
		if counter>0:

			dongles_status="%s%s\n" % (dongles_status,dongle)

			invalidState=False

			for status in validStatus:

				if dongle.lower().find(status)>=0:
			
					invalidState=True
					break

			if not invalidState:

				validDongles.append(dongle)

		if cfg.debug=="1":

			cfg.logs.writeDebugLog(dongle)

		counter+=1

	if len(validDongles)>0:

		if cfg.debug=="1":

			cfg.logs.writeDebugLog("")
			cfg.logs.writeDebugLog("Dongles passíveis de restart")
			cfg.logs.writeDebugLog("****************************")

		for dongle in validDongles:

			if cfg.debug=="1":

				cfg.logs.writeDebugLog(dongle)

		if len(calls)>0:

			if cfg.debug=="1":

				cfg.logs.writeDebugLog("")
				cfg.logs.writeDebugLog("Chamadas em andamento")
				cfg.logs.writeDebugLog("*********************")

			validCalls=[]
			active_calls=""

			for call in calls:

				active_calls="%s%s\n" % (active_calls,call)

				if cfg.debug=="1":

					cfg.logs.writeDebugLog(call)

				if call.lower().find(cfg.CHANTYPE.lower())>=0:

					validCalls.append(call)

			if len(validCalls)>0:

				if cfg.debug=="1":

					cfg.logs.writeDebugLog("")
					cfg.logs.writeDebugLog("Chamadas usando Dongles")
					cfg.logs.writeDebugLog("***********************")

				for call in validCalls:

					if cfg.debug=="1":

						cfg.logs.writeDebugLog(call)
				
			if cfg.debug=="1":

				cfg.logs.writeDebugLog("")
				cfg.logs.writeDebugLog("Analisando dongles")
				cfg.logs.writeDebugLog("******************")

				for channel in validDongles:

					dongle=channel.strip().split()[0]

					if cfg.debug=="1":

						cfg.logs.writeDebugLog("Analisando dongle %s." % dongle)

						needsRestart=True
		
					for call in validCalls:

						if call.find(dongle)>=0:

							needsRestart=False
							break

				if needsRestart:
		
					items=run_command([cfg.RASTERISK_PATH,"-rx","dongle show device state %s" % dongle])

					dongle_status=""

					for item in items:

						if item.find("State")>-1:

							dongle_status=item.strip().split(":")[1].strip()

					if dongle_status.lower() not in validStatusStr:	

						restarted=True

						if len(rChannels)>0:

							rChannels="%s, " % rChannels

						rChannels="%s%s" % (rChannels,dongle)

						DB.insertRestart(data,dongle,dongle_status,active_calls,dongles_status)

						restartData=time.strftime("%Y-%d-%d %H:%M:%S")
			
						cfg.logs.writeRestartLog("[%s] Dongle %s - Status %s." % (restartData,dongle,dongle_status))

						if dryrun:

							print "[%s] - Restart necessário - Status %s." % (dongle,dongle_status)

						if cfg.debug=="1":

							cfg.logs.writeDebugLog("[%s] - Restart necessário (%s)." % (dongle,dongle_status))

						if not dryrun:

							restart=run_command([cfg.RASTERISK_PATH,"-rx","dongle restart now %s" % dongle])

							if cfg.debug=="1":

								for restartLine in restart:

									cfg.logs.writeDebugLog("[%s] - Restart Output: %s." % (dongle,restartLine))
					else:

						if cfg.debug=="1":

							cfg.logs.writeDebugLog("[%s] - Restart não foi necessário. Status atualizado %s." % (dongle,dongle_status))

				else:		

						if cfg.debug=="1":

							cfg.logs.writeDebugLog("[%s] - Dongle em uso." % dongle)
			else:

				if cfg.debug=="1":

					cfg.logs.writeDebugLog("Não existem chamadas utilizando dongles em andamento.")
	
		else:

			if cfg.debug=="1":

				cfg.logs.writeDebugLog("Não existem chamadas em andamento.")
	
	else:

		if cfg.debug=="1":

			cfg.logs.writeDebugLog("Todos os dongles estão com status válido.")

	if cfg.debug=="1":

		cfg.logs.writeDebugLog("*********************************************************************")
		cfg.logs.writeDebugLog("*********************************************************************")

	return [restarted,rChannels]

