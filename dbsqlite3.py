#!/usr/bin/env python
# -*- coding: utf-8 -*-

# dbsqlite3.py
# Biblioteca de acesso ao DB
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-10-10
#

import sys
import time
import sqlite3

class DBSqlite3:

	def __init__(self,cfg):

		self.description = 'Sqlite3 Database'

		conn = sqlite3.connect(cfg.database)

		self.conn = conn 
		self.cfg = cfg

		self.initializeDB()

	def initializeDB(self):

		cursor=self.conn.cursor()

		sql="SELECT * from dbversion;"

		try: 

			cursor.execute(sql)

		except:

			print "Database inexistente"
			print "Criando estrutura inicial..."

			data=time.strftime("%Y-%m-%d %H:%M:%S")

			self.cfg.logs.writeLog("[%s] Criando banco de dados inicial." % data)

			sqls=[]

			sqlDbVersion='''CREATE TABLE dbversion (dbversion INTEGER)'''
			sqlInitVersion='''INSERT INTO dbversion (dbversion) VALUES (1)'''

			sqls.append(sqlDbVersion)
			sqls.append(sqlInitVersion)

			sqlChecks='''CREATE TABLE checks (data DATE NOT NULL, restart BOOLEAN DEFAULT 0, restart_info TEXT NOT NULL, dryrun BOOLEAN DEFAULT 0)'''
			sqlChecksIndex='''CREATE INDEX checks_data ON checks (data)'''

			sqls.append(sqlChecks)
			sqls.append(sqlChecksIndex)

			sqlRestarts='''CREATE TABLE restarts (data DATE NOT NULL, dongle VARCHAR(20) NOT NULL, dongle_status VARCHAR(20), active_calls TEXT NOT NULL, dongles_status TEXT NOT NULL);'''

			sqlRestartsIndex='''CREATE INDEX restarts_data ON restarts (data)'''
			sqlRestartsIndex2='''CREATE INDEX restarts_dongle ON restarts (dongle)'''

			sqls.append(sqlRestarts)
			sqls.append(sqlRestartsIndex)
			sqls.append(sqlRestartsIndex2)

			for sql in sqls:

				try:

					cursor.execute(sql)

				except:

					print "Erro na criação do database..."
					sys.exit()

		self.conn.commit()

	def insertCheck(self,data,restart,restart_info,dryrun):

		cursor=self.conn.cursor()

		if restart==True:

			restart=1
		else:

			restart=0

		if dryrun==True:

			dryrun=1
		else:

			dryrun=0

		sql="INSERT INTO checks (data,restart,restart_info,dryrun) VALUES('%s',%s,'%s',%s);" % (data,restart,restart_info,dryrun)

		try: 

			cursor.execute(sql)

		except:

			self.conn.rollback()
			return False

		self.conn.commit()
		return True

	def insertRestart(self,data,dongle,dongle_status,active_calls,dongles_status):

		cursor=self.conn.cursor()

		sql="INSERT INTO restarts (data,dongle,dongle_status,active_calls,dongles_status) VALUES('%s','%s','%s','%s','%s');" % (data,dongle,dongle_status,active_calls,dongles_status)

		try: 

			cursor.execute(sql)

		except:

			self.conn.rollback()
			return False

		self.conn.commit()
		return True

	def listChecks(self,cfg,startDate,endDate):

		cursor=self.conn.cursor()

		sql="SELECT * from checks WHERE data BETWEEN '%s' AND '%s' ORDER BY data;" % (startDate,endDate)

		data=[]

		try: 

			cursor.execute(sql)

		except:

			self.conn.rollback()

		print cursor.rowcount

		try:

			row=cursor.fetchone()

		except:

			data=[]

		else:

			while row:

				data.append(row)

				try:

					row=cursor.fetchone()
				except:

					row=None

		self.conn.commit()

		return data

	def listRestarts(self,cfg,startDate,endDate,dongle):

		cursor=self.conn.cursor()

		if dongle=="TODOS":

			sql="SELECT * from restarts WHERE data BETWEEN '%s' AND '%s' ORDER BY data;" % (startDate,endDate)

		else:
			sql="SELECT * from restarts WHERE data BETWEEN '%s' AND '%s' AND dongle='%s' ORDER BY data;" % (startDate,endDate,dongle)

		data=[]

		try: 

			cursor.execute(sql)

		except:

			self.conn.rollback()

		print cursor.rowcount

		try:

			row=cursor.fetchone()

		except:

			data=[]

		else:

			while row:

				data.append(row)

				try:

					row=cursor.fetchone()
				except:

					row=None

		self.conn.commit()

		return data
