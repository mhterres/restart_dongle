#!/usr/bin/env python
# -*- coding: utf-8 -*-

# defs.py
# Defs do sistema
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-10-10
#

def listRestarts(DB,cfg,startDate,endDate,dongle,format):

	print "Listando restart efetuados pelo sistema."

	data=DB.listRestarts(cfg,startDate,endDate,dongle)

	if format=="lista":

		print "Data                | Dongle               | Status"
		#      2015-10-10 13:00:00 | Modulo01             | Dialing....
	else:

		print "Data,Dongle,Status"

	for item in data:

		dongle=item[1].strip()
		dongle="%s%s" % (dongle,(" " * (20-len(dongle))))

		status=item[2].strip()
		status="%s%s" % (status,(" " * (20-len(status))))
		
		if format=="csv":

			print "%s,%s,%s" % (item[0],dongle.strip(),status.strip())
		else:

			print "%s | %s | %s " % (item[0],dongle,status)

	return True

def listChecks(DB,cfg,startDate,endDate,format):

	print "Listando checagens efetuadas pelo sistema."

	data=DB.listChecks(cfg,startDate,endDate)

	if format=="lista":

		print "Data                | DryRun | Restart | Dongles"
		#      2015-10-10 13:00:00 | Sim    | Nao     | 
	else:

		print "Data,DryRun,Restart,Dongles"

	for item in data:

		if item[3]==0:

			DryRun="Nao"
		else:

			DryRun="Sim"

		if item[1]==0:

			Restart="Nao"
		else:

			Restart="Sim"

		if format=="csv":

			print "%s,%s,%s,%s" % (item[0],DryRun,Restart,item[2])
		else:

			print "%s | %s    | %s     | %s" % (item[0],DryRun,Restart,item[2])

	return True
