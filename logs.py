#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
		logs.py
		Classe para logs do sistema
		2015/10/10
		Marcelo Hartmann Terres <mhterres@gmail.com>
"""

import os
import sys
import ConfigParser

class Logs:

	def __init__(self,logFile,restartFile,errorFile,debugFile):

		self.description = 'Logs do sistema'

		self.logfile=open(logFile,'a+')
		self.restartfile=open(restartFile,'a+')
		self.errorfile=open(errorFile,'a+')
		self.debugfile=open(debugFile,'a+')

	def writeLog(self,text):

		self.logfile.write("%s\n" % text)
		self.logfile.flush()

	def writeRestartLog(self,text):

		self.restartfile.write("%s\n" % text)
		self.restartfile.flush()

	def writeErrorLog(self,text):

		self.errorfile.write("%s\n" % text)
		self.errorfile.flush()

	def writeDebugLog(self,text):

		self.debugfile.write("%s\n" % text)
		self.debugfile.flush()

