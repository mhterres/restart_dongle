#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
		log.py
		Classe para log do sistema
		2015/10/10
		Marcelo Hartmann Terres <mhterres@gmail.com>
"""

import os
import sys
import ConfigParser

class Log:

	def __init__(self):

		self.description = 'Log'

		configuration = ConfigParser.RawConfigParser()
		configuration.read('/etc/restart_dongle/restart_dongle.conf')

		# geral
		self.running=configuration.get('geral','running')
		self.debug=configuration.get('geral','debug')

		# asterisk
		self.RASTERISK_PATH=configuration.get('asterisk','RASTERISK_PATH')
		self.CHANTYPE=configuration.get('asterisk','CHANTYPE')

		self.logfile="/var/restart_dongle/checks.log"
		self.debugfile="/var/restart_dongle/debug.log"

		self.database="/var/restart_dongle/db/restart_dongle.db"

