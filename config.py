#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
		config.py
		Classe da configuração do sistema
		2015/10/10
		Marcelo Hartmann Terres <mhterres@gmail.com>
"""

import os
import sys
import ConfigParser

from logs import Logs

class Config:

	def __init__(self):

		self.description = 'Configurations'

		configuration = ConfigParser.RawConfigParser()
		configuration.read('/etc/restart_dongle/restart_dongle.conf')

		# geral
		self.running=configuration.get('geral','running')
		self.debug=configuration.get('geral','debug')

		# asterisk
		self.RASTERISK_PATH=configuration.get('asterisk','RASTERISK_PATH')
		self.CHANTYPE=configuration.get('asterisk','CHANTYPE')

		self.logfile="/var/restart_dongle/log/checks.log"
		self.restartfile="/var/restart_dongle/log/restarts.log"
		self.errorfile="/var/restart_dongle/log/error.log"
		self.debugfile="/var/restart_dongle/log/debug.log"

		self.database="/var/restart_dongle/db/restart_dongle.db"

		self.logs=Logs(self.logfile,self.restartfile,self.errorfile,self.debugfile)

