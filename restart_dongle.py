#!/usr/bin/env python
# -*- coding: utf-8 -*-

# restart_dongle.py
# Controle de restart de dongles travados
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-10-10
#

import os
import sys
import time
import getopt
import atexit
import datetime

import defs
import dongles
from logs import Logs
from config import Config
from dbsqlite3 import DBSqlite3

global DB
global cfg
global pidfile

def exitDaemon():

    data=time.strftime("%Y-%m-%d %H:%M:%S")

    cfg.logs.writeLog("[%s] Encerrando Daemon." % data)

    os.unlink(pidfile)
    print "Encerrando Daemon..."

    if cfg.debug==1:

      cfg.logs.writeDebugLog("[%s] Encerrado Daemon." % data)

def main():

    while True:

        data=time.strftime("%Y-%m-%d %H:%M:%S")

        cfg.logs.writeLog("[%s] Checando Dongles." % data)

        returnData=dongles.verifyDongles(data,cfg,DB,False)

        if not DB.insertCheck(data,returnData[0],returnData[1],False):

            cfg.logs.writeErrorLog("[%s] Não foi possível adicionar o registro de checagem no DB." % data)

        time.sleep(int(cfg.running))

def operations(argv,validOperations):

    startDate=""
    endDate=""
    dongle=""
    operation=""
    format=""

    try:

        opts, args = getopt.getopt(argv,"hs:e:c:o:f:d",["start=","end=","dongle=","operation=","format=","daemon"])

    except getopt.GetoptError:

        print "Parâmetros incorretos. Rode ./restart_dongle.py -h para ajuda."
        sys.exit(2)

    for opt, arg in opts:

        if opt == '-h':

            print "restart_dongle.py -s [data inicial] -e [data final] -c [dongle] -o [operação] -f [formato]"
            print "Opções: "
            print " -h: ajuda"
            print " -s: data inicial para consulta (default - hoje)"
            print " -e: data final para consulta"
            print " -c: nome do dongle a ser consultado, conforme configurado no dongles.conf (default - Todos)"
            print " -o: consulta a ser efetuada (%s)" % validOperations
            print " -f: formato de retorno da consulta: C - CSV, L - lista (default - lista)." 
            print " -d: executar como daemon"
            print "Exemplo: restart_dongle.py -s 2015-10-09 -e 2015-10-10 -d ch01 -o restart"
            sys.exit(2)

        elif opt in ("-s", "--start"):

            startDate = arg

        elif opt in ("-e", "--end"):

            endDate = arg

        elif opt in ("-c", "--dongle"):

            dongle = arg

        elif opt in ("-o", "--operation"):

            operation = arg

        elif opt in ("-f", "--format"):

            format = arg

    if startDate=="" and endDate=="":

        startDate=datetime.date.today()
        endDate=datetime.date.today()

    if dongle=="":

        dongle="TODOS"

    if operation=="":

        operation="restart"

    if format=="":

        format="lista"
    try:

        s_dt=datetime.datetime.strptime("%s 00:00:00" % startDate,"%Y-%m-%d %H:%M:%S")

    except:

        print "Start date %s is invalid." % startDate
        sys.exit(1)

    try:

        e_dt=datetime.datetime.strptime("%s 23:59:59" % endDate,"%Y-%m-%d %H:%M:%S")

    except:

        print "End date %s is invalid." % endDate
        sys.exit(1)

    if e_dt<s_dt:

        print "End date must be greater than start date."
        sys.exit(1)

    if operation not in validOperations:

        print "Operação %s é inválida. Operações válidas são: %s." % (operation,validOperations)
        sys.exit(1)

    if format not in "csv, lista":

        print "Formato %s é inválid. Formatos válidos são: csv, lista." 
        sys.exit(1)


    startDate = "%s 00:00:00" % startDate
    endDate = "%s 23:59:59" % endDate

    if operation in "check,restart":

        print "Operação: %s - Dongle: %s - Format %s - Período: %s à %s" % (operation,dongle,format,startDate,endDate)
    else:

        print "Operação: %s - Dongle: %s - Período: %s à %s" % (operation,dongle,startDate,endDate)

    if operation=="restart":

        defs.listRestarts(DB,cfg,startDate,endDate,dongle,format)

    elif operation=="check":

        defs.listChecks(DB,cfg,startDate,endDate,format)

    elif operation=="dryrun":

        data=time.strftime("%Y-%m-%d %H:%M:%S")

        cfg.logs.writeLog("[%s] Checando Dongles(DRYRUN)." % data)
        returnData=dongles.verifyDongles(data,cfg,DB,True)

        DB.insertCheck(data,returnData[0],returnData[1],True)

if __name__ == "__main__":

    DAEMON=False

    validOperations='restart,check,dryrun'

    try:

        opts, args = getopt.getopt(sys.argv[1:],"hs:e:c:o:f:d",["start=","end=","dongle=","operation=","format=","daemon"])

    except getopt.GetoptError:

        print "Parâmetros incorretos. Rode ./restart_dongle.py -h para ajuda."
        sys.exit(2)

    for opt, arg in opts:

        if opt == '-d':

            DAEMON=True

    cfg=Config()

    DB=DBSqlite3(cfg)

    if DAEMON:

        pid = str(os.getpid())
        pidfile = "/var/run/restart_dongle.pid"

        if os.path.isfile(pidfile):

            print "Restart_dongle já está rodando. Saindo..." 
            sys.exit()

        else:

            file(pidfile, 'w').write(pid)

        atexit.register(exitDaemon)

        data=time.strftime("%Y-%m-%d %H:%M:%S")

        print "Iniciando Daemon Restart Dongle."
        cfg.logs.writeLog("[%s] Iniciando Daemon." % data)

        if cfg.debug=="1":

          cfg.logs.writeDebugLog("[%s] Iniciando Daemon." % data)


        main()

    else:

        operations(sys.argv[1:],validOperations)

    
